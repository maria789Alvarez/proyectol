<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/', ['as' => 'inicio', 'uses' => 'HomeController@index']);
 
//Route::get('/', ['as' => 'inicio', 'uses' => 'MainController@inicio']);

// Autenticación

//Route::get('/register', 'Auth\RegisterController@register')->name('register');

