
/*$(document).ready(function(){
  checkForm("#nameform");
  $("#aliados").on('submit', function(e){
    e.preventDefault();
    checkForm("#nameform")
  });
});*/

// function changeOficio(){
//   $("#ocupacion").children('option[value="0"]').remove();
// }

/**
 * Controlador de la validación
 * Comprueba el contenido del formulario ante los siguientes eventos: la modificación de un campo (change) o la pulsación de cualquier tecla (keydown). Si todas las condiciones que declaramos se cumplen el botón Enviar se activará y podremos enviar el formulario. En caso contrario es desactivado y no es posible enviarlo.
 */
function checkForm(idForm){
  $(idForm + " *").on("change keydown keyup", function(){
    enableSubmit();

    if(!checkInput("#nombres", namePattern)){
      disableSubmit();
    }

    if(!checkInput("#apellidos", namePattern)){
      disableSubmit();
    }

    /*if(!checkInput("#email", emailPattern)){
      disableSubmit();
    }*/

    if(!checkInput("#celular", celularPattern)){
      disableSubmit();
    }

    if(!checkInput("#dream", dreamPattern)){
      disableSubmit();
    }

    if(!checkSelect("#ocupacion")){
      disableSubmit();
    }

    if(!checkRadioBox("[name=banco]")){
      disableSubmit();
    }

    if(!checkRadioTerminos()){
      disableSubmit();
    }

    if(!checkRadioAutorizacion()){
      disableSubmit();
    }

    if(!checkInputMoney("#necesidad", dineroPattern)){
      disableSubmit();
    }

    if(!checkInputMoney("#ingresos", dineroPattern)){
      disableSubmit();
    }

    formatearMoney("#necesidad");
    formatearMoney("#ingresos");
  });
}

function formatearMoney(input){
  value = $(input).val().toString();
  value = value.replace(/\D/g,'');

  if(!isNaN(value) && value != ''){
    value = parseInt(value).toString();
    value = Number(value).toLocaleString('en');
    $(input).val('$ ' + value);
  }
}

// Validación de formulario
var namePattern = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{4,30}$";
var emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$";
var celularPattern = "^[0-9]{10}$";
var dreamPattern = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{5,150}$";
var dineroPattern = "^[0-9]{6,9}$";

// inputs
function checkInputMoney(idInput, pattern){
  value = $(idInput).val();
  response = value.replace(/\D/g,'');
  response = response.match(pattern) ? true : false;
  gestionErrorFormato(idInput, response);
  return response;
}

// inputs
function checkInput(idInput, pattern){
  response = $(idInput).val().match(pattern) ? true : false;
  gestionErrorFormato(idInput, response);
  return response;
}

// textarea
function checkTextarea(idText){
  response = $(idText).val().length > 12 ? true : false;  
  gestionErrorFormato(idText, response);
  return response;
}

// radiobuton y checkboxes
function checkRadioBox(nameRadioBox){
  response = $(nameRadioBox).is(":checked") ? true : false;
  gestionErrorSeleccion(nameRadioBox, response);
  return response;
}

function checkRadioTerminos(){
  id = '#check2';
  response = $(id).is(":checked") ? true : false;
  if(response){
    $(id + "Error").html("");
  }
  else{
    $(id + "Error").html("Error, debe aceptar los términos para continuar");
  }
  return response;
}

function checkRadioAutorizacion(){
  id = '#check1';
  response = $(id).is(":checked") ? true : false;
  if(response){
    $(id + "Error").html("");
  }
  else{
    $(id + "Error").html("Error, debe dar su autorización para continuar");
  }
  return response;
}

// select
function checkSelect(idSelect){
  response = parseInt($(idSelect).val()) > 0 ? true : false;
  gestionErrorSeleccion(idSelect, response);
  return response;
}

function gestionErrorFormato(id, validacion){
  if(validacion){
    $(id + "Error").html("");
  }
  else{
    $(id + "Error").html("Error, formato inválido");
  }
}

function gestionErrorSeleccion(id, validacion){
  if(validacion){
    $(id + "Error").html("");
  }
  else{
    $(id + "Error").html("Error, debe seleccionar una opción");
  }
}