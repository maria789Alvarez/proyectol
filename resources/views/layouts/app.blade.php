@include('layouts.head')
  <body>
    <div class="page">
      <!-- Page Header-->
      @include('layouts.header')

      @section('content')
      @show
      
      <!-- Page Footer-->
     
    </div>

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{ URL::to('/js/core.min.js') }}"></script>
    <script src="{{ URL::to('/js/script.js') }}"></script>
  </body>
</html>