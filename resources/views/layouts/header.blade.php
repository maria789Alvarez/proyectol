      <header class="section page-header">
        <?php
          function activeMenu($url){
            return request()->is($url) ? 'active' : '';
          }
        ?>

        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand"><a class="brand" href=""><img src="images/logo-default-191x52.png" alt="" width="191" height="52"/></a></div>
                </div>
                <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Share-->
                    <!--
                    <div class="rd-navbar-share">
                      <div class="rd-navbar-share-icon fl-bigmug-line-share27" data-rd-navbar-toggle=".rd-navbar-share-list"></div>
                      <ul class="list-inline rd-navbar-share-list">
                        <li class="rd-navbar-share-list-item">
                          <a target="_blank" class="icon fa fa-facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//linkedban.com/"></a>
                        </li>
                        <li class="rd-navbar-share-list-item">
                          <a target="_blank" class="icon fa fa-twitter" href="https://twitter.com/home?status=http%3A//linkedban.com/"></a>
                        </li>
                        <li class="rd-navbar-share-list-item">
                          <a target="_blank" class="icon fa fa-linkedin-square" href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//linkedban.com/&title=linkedban&summary=&source="></a>
                        </li>
                        <li class="rd-navbar-share-list-item"><a target="_blank" class="icon fa fa-instagram" href="https://www.instagram.com/linkedban_/"></a></li>
                        <li class="rd-navbar-share-list-item"><a target="_blank" class="icon fa fa-youtube" href="https://www.youtube.com/channel/UCYQIT1dlygqKD8SKnD__C_A"></a></li>
                      </ul>
                    </div>
                    -->
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="rd-nav-item"><a class="rd-nav-link" href="{{ route('inicio') }}">Inicio</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="">Quienes Somos</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="">Contáctenos</a></li>

                    @auth
                        <li class="rd-nav-item" style="border-color: #4f00ff; border-width: 3px; border-style: solid; padding-top: 2px; padding-left: 6px; padding-bottom: 2px; padding-right: 6px;">
                          <a class="rd-nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Cerrar sesión
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                        </li>
                      @else
                        <li class="rd-nav-item" style="background-color: white; border-color: #4f00ff; border-width: 3px; border-style: solid; padding-top: 2px; padding-left: 6px; padding-bottom: 2px; padding-right: 6px; font-weight: bolder;">
                          <a class="rd-nav-link" href="{{ route('login') }}" style=" padding-bottom: 2px; padding-top: 2px;">
                            Ingresar
                          </a>
                        </li>
                        @if (Route::has('register'))
                          <li class="rd-nav-item"><a class="rd-nav-link" href="{{ route('register') }}">Regístrate</a></li>
                        @endif
                      @endauth
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
