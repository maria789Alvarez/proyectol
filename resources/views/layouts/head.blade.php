<!DOCTYPE html>
<html class="wide wow-animation" lang="es">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140965502-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-140965502-1');
        </script>

        <title>ProyectoL - @yield('title')</title>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <link rel="icon" href="{{ URL::to('/images/p.png') }}" type="image/x-icon">
        <!-- Stylesheets-->
        <link rel="stylesheet" href="{{ URL::to('/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ URL::to('/css/fonts.css') }}">
        <link rel="stylesheet" href="{{ URL::to('/css/google.css') }}">
        <link rel="stylesheet" href="{{ URL::to('/css/style.css') }}">
        <!--[if lt IE 10]>
        <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;">
            <a href="http://windows.microsoft.com/en-US/internet-explorer/">
                <img src="{{ URL::to('/images/warning_bar_0000_us.jpg') }}" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
            </a>
        </div>
        <script src="{{ URL::to('/js/html5shiv.min.js') }}"></script>
        <![endif]-->
        <!-- recaptcha -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>