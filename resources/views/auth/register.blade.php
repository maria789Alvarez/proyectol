@section('head')

@extends('layouts.app')

@section('title', 'Registrarse')


@section('content')
  <section class="section section-sm section-first bg-default">
    <h2>Registrarse</h2>

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6 order-md-2 mb-4 ">
          <form class="form-horizontal" method="POST" action="{{ route('register') }}" novalidate>
            {!! csrf_field() !!}

            <div class="col-md-12 mb-6">
              <div class="form-group{{ $errors->has('nombres') ? ' has-error' : '' }}">
                <label for="nombres" class="col-md-4 control-label">Nombres</label>
                <div class="col-md-12">
                  <input id="nombres" type="text" class="form-control" name="nombres" value="{{ old('nombres') }}" required autofocus>

                  @if ($errors->has('nombres'))
                    <span class="help-block">
                      <strong>{{ $errors->first('nombres') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-md-12 mb-6">
              <div class="form-group{{ $errors->has('apellidos') ? ' has-error' : '' }}">
                <label for="apellidos" class="col-md-4 control-label">Apellidos</label>
                <div class="col-md-12">
                  <input id="apellidos" type="text" class="form-control" name="apellidos" value="{{ old('apellidos') }}" required autofocus>

                  @if ($errors->has('apellidos'))
                    <span class="help-block">
                      <strong>{{ $errors->first('apellidos') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>
            

            <div class="col-md-12 mb-6">
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-6 control-label">Correo electrónico</label>
                <div class="col-md-12">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                  @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-md-12 mb-6">
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Contraseña</label>
                <div class="col-md-12">
                  <input id="password" type="password" class="form-control" name="password" required>

                  @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-md-12 mb-6">
              <div class="form-group">
                <label for="password-confirm" class="col-md-5 control-label">Confirmar contraseña</label>
                <div class="col-md-12">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
              </div>
            </div>

        <!--     <div class="col-md-12 mb-6">
              <div class="form-group">
                <label class="control control-radio">
                  <input type="radio" name="autorizacion" value="1">
                  Doy mi <a href="">autorización para el tratamiento de datos personales<span></span></a>
                  <div class="control_indicator"></div>
                </label>
                
                @if ($errors->has('autorizacion'))
                  <br><span class="help-block">
                    <strong>{{ $errors->first('autorizacion') }}</strong>
                  </span>
                @endif
              </div>
            </div> -->

        <!--     <div class="col-md-12 mb-6">
              <div class="form-group">
                <label class="control control-radio">
                  <input type="radio" name="terminos" value="1">
                  Acepto los <a href="">términos y Condiciones<span></span></a>
                  <div class="control_indicator"></div>
                </label>
                
                @if ($errors->has('terminos'))
                  <br><span class="help-block">
                    <strong>{{ $errors->first('terminos') }}</strong>
                  </span>
                @endif
              </div>
            </div> -->

         <!--    <div class="col-md-12 mb-6">
              <div class="form-group">
                <div class="col-md-12">
                  <div class="g-recaptcha" data-sitekey="6Ld3yLkUAAAAANIHiE_GFqRkXk3can20iadshokU"></div>
                </div>
                <div class="col-md-6">
                  @if ($errors->has('reCaptcha'))
                    <span class="help-block">
                      <strong>{{ $errors->first('reCaptcha') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            </div> -->

            <div class="form-group">
              <div class="col-md-12 col-md-offset-8">
                <button type="submit" class="btn btn-primary">
                  Registrarse
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </section>
@stop