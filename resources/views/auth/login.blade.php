@extends('layouts.app')
@section('title', 'Iniciar sesión')

@section('content')
<h2>Iniciar sesión</h2>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6 order-md-2 mb-4 ">
      <form class="form-horizontal" method="POST" action="{{ route('login') }}" novalidate>
        {!! csrf_field() !!}

        <div class="col-md-12 mb-6">
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-md-12">
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo electrónico">

              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>
          </div>
        </div>

        <div class="col-md-12 mb-6">
          <div class="form-group{{ ($errors->has('password') || $errors->has('credenciales')) ? ' has-error' : '' }}">
            <div class="col-md-12">
              <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

              @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
              @if ($errors->has('credenciales'))
                <span class="help-block">
                  <strong>{{ $errors->first('credenciales') }}</strong>
                </span>
              @endif
            </div>
          </div>
        </div>

        <div class="col-md-12 mb-6">
          <div class="form-group">
{{-- JAAM boton remember
            <div class="col-md-4 col-md-offset-4">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuérdame
                </label>
              </div>
            </div>
--}}
            <div class="col-md-8 mb-6 row justify-content-right" style="margin-top: 5px;">
              <label style="padding-left: 15px;">
                ¿No tiene una cuenta? <a class="btn btn-link" href="{{ route('register') }}"> Cree una.</a>
              </label>
              <label>
                <a class="btn btn-link" href="{{ route('password.request') }}"> ¿Olvidaste tu contraseña?</a>
              </label>
            </div>
          </div>
        </div>

        <div class="col-md-12 mb-6">
          <div class="form-group">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary pull-right">
                Ingresar
              </button>

            </div>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
